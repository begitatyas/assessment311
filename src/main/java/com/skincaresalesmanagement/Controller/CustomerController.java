package com.skincaresalesmanagement.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skincaresalesmanagement.Model.Customer;
import com.skincaresalesmanagement.Service.CustomerService;

@RestController
@RequestMapping("/master/")

public class CustomerController {
	
	private final CustomerService customerService;

	  public CustomerController(CustomerService customerService) {
	    this.customerService = customerService;
	  }

	  @GetMapping("customer")
	  public List<Customer> getAllCustomer() {
	    return customerService.getAllCustomer();
	  }
	  
	  @GetMapping("customer/{id}")
	  public ResponseEntity<Customer> findById(@PathVariable Long id) {
	    Optional<Customer> customer = customerService.findByCustomerId(id);
	    return customer.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	  }

	  @PostMapping("customer/add")
	  public Customer saveCustomer(@RequestBody Customer customer) {
	    return customerService.saveCustomer(customer);
	  }
	  
	  @PostMapping("customer/{id}")
	  public void deleteCustomer(@PathVariable Long id) {
		  customerService.deleteCustomer(id);
	  }

}
