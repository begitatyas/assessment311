package com.skincaresalesmanagement.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skincaresalesmanagement.Model.SkincareProduct;
import com.skincaresalesmanagement.Service.ProductService;

@RestController
@RequestMapping("/master/")
public class ProductController {

	private final ProductService productService;

	public ProductController(ProductService productService) {
	    this.productService = productService;
	  }

	@GetMapping("product")
	public List<SkincareProduct> getAllProduct() {
		return productService.getAllProduct();
	}

	@GetMapping("product/{id}")
	public ResponseEntity<SkincareProduct> findById(@PathVariable Long id) {
		Optional<SkincareProduct> product = productService.findByProductId(id);
		return product.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PostMapping("product/add")
	public SkincareProduct saveProduct(@RequestBody SkincareProduct productCategory) {
		return productService.saveProduct(productCategory);
	}

	@PostMapping("product/{id}")
	public void deleteProduct(@PathVariable Long id) {
		productService.deleteProduct(id);
	}

}
