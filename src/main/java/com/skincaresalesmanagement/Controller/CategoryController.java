package com.skincaresalesmanagement.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skincaresalesmanagement.Model.SkincareCategory;

import com.skincaresalesmanagement.Service.CategoryService;

@RestController
@RequestMapping("/master/")

public class CategoryController {

	private final CategoryService categoryService;

	  public CategoryController(CategoryService categoryService) {
	    this.categoryService = categoryService;
	  }

	  @GetMapping("category")
	  public List<SkincareCategory> getAllCategory() {
	    return categoryService.getAllCategory();
	  }
	  
	  @GetMapping("category/{id}")
	  public ResponseEntity<SkincareCategory> findById(@PathVariable Long id) {
	    Optional<SkincareCategory> category = categoryService.findByCategoryId(id);
	    return category.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	  }

	  @PostMapping("category/add")
	  public SkincareCategory saveCategory(@RequestBody SkincareCategory skincareCategory) {
	    return categoryService.saveCategory(skincareCategory);
	  }
	  
	  @PostMapping("category/{id}")
	  public void deleteCategory(@PathVariable Long id) {
		  categoryService.deleteCategory(id);
	  }
	  
}
