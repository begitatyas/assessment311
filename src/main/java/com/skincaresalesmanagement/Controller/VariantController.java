package com.skincaresalesmanagement.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skincaresalesmanagement.Model.SkincareVariant;
import com.skincaresalesmanagement.Service.VariantService;

@RestController
@RequestMapping("/master/")
public class VariantController {
	private final VariantService variantService;

	  public VariantController(VariantService variantService) {
	    this.variantService = variantService;
	  }

	  @GetMapping("variant")
	  public List<SkincareVariant> getAllVariant() {
	    return variantService.getAllVariant();
	  }
	  
	  @GetMapping("variant/{id}")
	  public ResponseEntity<SkincareVariant> findById(@PathVariable Long id) {
	    Optional<SkincareVariant> variant = variantService.findByVariantId(id);
	    return variant.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	  }

	  @PostMapping("variant/add")
	  public SkincareVariant saveVariant(@RequestBody SkincareVariant skincareVariant) {
	    return variantService.saveVariant(skincareVariant);
	  }
	  
	  @PostMapping("category/{id}")
	  public void deleteVariant(@PathVariable Long id) {
		  variantService.deleteVariant(id);
	  }
	  
}
