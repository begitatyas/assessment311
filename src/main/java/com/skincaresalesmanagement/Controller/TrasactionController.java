package com.skincaresalesmanagement.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skincaresalesmanagement.Model.Transaction;
import com.skincaresalesmanagement.Service.TransactionService;

@RestController
@RequestMapping("/master/")

public class TrasactionController {
	private final TransactionService transactionService;

	  public TrasactionController(TransactionService transactionService) {
	    this.transactionService = transactionService;
	  }

	  @GetMapping("transaction")
	  public List<Transaction> getAllTransaction() {
	    return transactionService.getAllTransaction();
	  }
	  
	  @GetMapping("transaction/{id}")
	  public ResponseEntity<Transaction> findById(@PathVariable Long id) {
	    Optional<Transaction> transaction = transactionService.findByTransactionId(id);
	    return transaction.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	  }

	  @PostMapping("transaction/add")
	  public Transaction saveTransaction(@RequestBody Transaction transaction) {
	    return transactionService.saveTransaction(transaction);
	  }
	  
	  @PostMapping("transaction/{id}")
	  public void deleteTransaction(@PathVariable Long id) {
		  transactionService.deleteTransaction(id);
	  }
}
