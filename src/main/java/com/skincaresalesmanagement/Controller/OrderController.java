package com.skincaresalesmanagement.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skincaresalesmanagement.Model.OrderDetail;
import com.skincaresalesmanagement.Service.OrderService;

@RestController
@RequestMapping("/master/")
public class OrderController {

	private final OrderService orderService;

	public OrderController(OrderService orderService) {
	    this.orderService = orderService;
	  }

	@GetMapping("order")
	public List<OrderDetail> getAllCategory() {
		return orderService.getAllOrder();
	}

	@GetMapping("order/{id}")
	public ResponseEntity<OrderDetail> findById(@PathVariable Long id) {
		Optional<OrderDetail> order = orderService.findByOrderId(id);
		return order.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@PostMapping("order/add")
	public OrderDetail saveOrder(@RequestBody OrderDetail orderDetail) {
		return orderService.saveOrder(orderDetail);
	}

	@PostMapping("order/{id}")
	public void deleteOrder(@PathVariable Long id) {
		orderService.deleteOrder(id);
	}

}
