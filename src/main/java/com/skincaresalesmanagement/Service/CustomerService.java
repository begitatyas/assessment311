package com.skincaresalesmanagement.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.skincaresalesmanagement.Model.Customer;
import com.skincaresalesmanagement.Repository.CustomerRepository;

@Service
public class CustomerService {
	 private final CustomerRepository customerRepository;
	 public CustomerService(CustomerRepository customerRepository) {
		    this.customerRepository = customerRepository;
		  }

		  public List<Customer> getAllCustomer() {
		    return customerRepository.findByIsActive();
		  }

		  public Optional<Customer> findByCustomerId(Long id) {
		    return customerRepository.findById(id);
		  }

		  public Customer saveCustomer(Customer customer) {
		    return customerRepository.save(customer);
		  }

		  public void deleteCustomer(Long id) {
			  customerRepository.findById(id);
		  }

}
