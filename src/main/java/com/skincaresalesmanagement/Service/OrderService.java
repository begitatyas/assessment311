package com.skincaresalesmanagement.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.skincaresalesmanagement.Model.OrderDetail;
import com.skincaresalesmanagement.Repository.OrderRepository;

@Service
public class OrderService {
	private final OrderRepository orderRepository;

	public OrderService(OrderRepository orderRepository) {
		    this.orderRepository = orderRepository;
		  }

	public List<OrderDetail> getAllOrder() {
		return orderRepository.findByIsActive();
	}

	public Optional<OrderDetail> findByOrderId(Long id) {
		return orderRepository.findById(id);
	}

	public OrderDetail saveOrder(OrderDetail orderCategory) {
		return orderRepository.save(orderCategory);
	}

	public void deleteOrder(Long id) {
		orderRepository.findById(id);
	}
}
