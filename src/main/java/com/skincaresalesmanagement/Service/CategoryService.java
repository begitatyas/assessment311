package com.skincaresalesmanagement.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.skincaresalesmanagement.Model.SkincareCategory;
import com.skincaresalesmanagement.Repository.CategoryRepository;

@Service
public class CategoryService {
	 private final CategoryRepository categoryRepository;
	 public CategoryService(CategoryRepository categoryRepository) {
		    this.categoryRepository = categoryRepository;
		  }

		  public List<SkincareCategory> getAllCategory() {
		    return categoryRepository.findByIsActive();
		  }

		  public Optional<SkincareCategory> findByCategoryId(Long id) {
		    return categoryRepository.findById(id);
		  }

		  public SkincareCategory saveCategory(SkincareCategory skincareCategory) {
		    return categoryRepository.save(skincareCategory);
		  }

		  public void deleteCategory(Long id) {
			  categoryRepository.findById(id);
		  }
		  
}
