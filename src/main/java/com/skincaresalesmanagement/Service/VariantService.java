package com.skincaresalesmanagement.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.skincaresalesmanagement.Model.SkincareVariant;
import com.skincaresalesmanagement.Repository.VariantRepository;

@Service
public class VariantService {
	private final VariantRepository variantRepository;

	public VariantService(VariantRepository variantRepository) {
		    this.variantRepository = variantRepository;
		  }

	public List<SkincareVariant> getAllVariant() {
		return variantRepository.findByIsActive();
	}

	public Optional<SkincareVariant> findByVariantId(Long id) {
		return variantRepository.findById(id);
	}

	public SkincareVariant saveVariant(SkincareVariant skincareVariant ) {
		return variantRepository.save(skincareVariant);
	}

	public void deleteVariant(Long id) {
		variantRepository.findById(id);
	}
}
