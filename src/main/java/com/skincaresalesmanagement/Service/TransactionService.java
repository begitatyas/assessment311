package com.skincaresalesmanagement.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.skincaresalesmanagement.Model.Transaction;
import com.skincaresalesmanagement.Repository.TransactionRepository;

@Service
public class TransactionService {
	private final TransactionRepository transactionRepository;

	public TransactionService(TransactionRepository transactionRepository) {
		    this.transactionRepository = transactionRepository;
		  }

	public List<Transaction> getAllTransaction() {
		return transactionRepository.findByIsActive();
	}

	public Optional<Transaction> findByTransactionId(Long id) {
		return transactionRepository.findById(id);
	}

	public Transaction saveTransaction(Transaction transaction) {
		return transactionRepository.save(transaction);
	}

	public void deleteTransaction(Long id) {
		transactionRepository.findById(id);
	}
}
