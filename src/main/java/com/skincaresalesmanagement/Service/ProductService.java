package com.skincaresalesmanagement.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.skincaresalesmanagement.Model.SkincareProduct;
import com.skincaresalesmanagement.Repository.ProductRepository;

@Service
public class ProductService {
	private final ProductRepository productRepository;

	public ProductService(ProductRepository productRepository) {
		    this.productRepository = productRepository;
		  }

	public List<SkincareProduct> getAllProduct() {
		return productRepository.findByIsActive();
	}

	public Optional<SkincareProduct> findByProductId(Long id) {
		return productRepository.findById(id);
	}

	public SkincareProduct saveProduct(SkincareProduct skincareProduct) {
		return productRepository.save(skincareProduct);
	}

	public void deleteProduct(Long id) {
		productRepository.findById(id);
	}
}
