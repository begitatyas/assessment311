package com.skincaresalesmanagement.Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "variant")
public class SkincareVariant {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
		
	@Column(name = "category_id")
	private Long categoryId;
		
	@Column(name = "variant_initial")
	private String variantInitial;
		
	@Column(name = "variant_name")
	private String variantName;
		
	@Column(name = "is_active")
	private Boolean isActive;
		
	@Column(name = "create_by")
	private String createBy;
		
	@Column(name = "create_date")
	private Date createDate;
		
	@Column(name = "modify_by")
	private String modifyBy;
		
	@Column(name = "modify_date")
	private Date modifyDate;
	
	@ManyToOne 
	@JoinColumn(name = "category_id", insertable =false, updatable = false)
	private SkincareCategory skincareCategory;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getVariantInitial() {
		return variantInitial;
	}

	public void setVariantInitial(String variantInitial) {
		this.variantInitial = variantInitial;
	}

	public String getVariantName() {
		return variantName;
	}

	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public SkincareCategory getSkincareCategory() {
		return skincareCategory;
	}

	public void setSkincareCategory(SkincareCategory skincareCategory) {
		this.skincareCategory = skincareCategory;
	}

	
	
	
}
