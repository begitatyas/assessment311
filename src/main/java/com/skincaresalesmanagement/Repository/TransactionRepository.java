package com.skincaresalesmanagement.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.skincaresalesmanagement.Model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
	@Query(value ="SELECT * FROM transaction WHERE is_active = true", nativeQuery = true)
	List<Transaction> findByIsActive ();
	
	Optional<Transaction> findByTransactionId (Long TransactionId);

}
