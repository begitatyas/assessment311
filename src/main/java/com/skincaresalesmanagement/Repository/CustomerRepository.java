package com.skincaresalesmanagement.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.skincaresalesmanagement.Model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	@Query(value ="SELECT * FROM customer WHERE is_active = true", nativeQuery = true)
	List<Customer> findByIsActive ();
	
	Optional<Customer> findByCustomerId (Long CategoryId);

}
