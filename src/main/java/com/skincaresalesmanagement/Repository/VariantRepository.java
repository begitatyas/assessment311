package com.skincaresalesmanagement.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.skincaresalesmanagement.Model.SkincareVariant;

public interface VariantRepository extends JpaRepository<SkincareVariant, Long>  {

	@Query(value ="SELECT * FROM variant WHERE is_active = true", nativeQuery = true)
	List<SkincareVariant> findByIsActive ();
	
	Optional<SkincareVariant> findByVariantId (Long VariantId);
}
