package com.skincaresalesmanagement.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.skincaresalesmanagement.Model.OrderDetail;

public interface OrderRepository extends JpaRepository<OrderDetail, Long>{
	@Query(value ="SELECT * FROM orders WHERE is_active = true", nativeQuery = true)
	List<OrderDetail> findByIsActive ();
	
	Optional<OrderDetail> findByOrderId (Long OrderId);

}
