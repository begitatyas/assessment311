package com.skincaresalesmanagement.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.skincaresalesmanagement.Model.SkincareProduct;

public interface ProductRepository extends JpaRepository<SkincareProduct, Long> {
	
	@Query(value ="SELECT * FROM product WHERE is_active = true", nativeQuery = true)
	List<SkincareProduct> findByIsActive ();
	
	Optional<SkincareProduct> findByCategoryId (Long VariantId);

}
