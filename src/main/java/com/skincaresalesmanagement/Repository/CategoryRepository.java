package com.skincaresalesmanagement.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.skincaresalesmanagement.Model.SkincareCategory;

public interface CategoryRepository extends JpaRepository<SkincareCategory, Long> {
	
	@Query(value ="SELECT * FROM category WHERE is_active = true", nativeQuery = true)
	List<SkincareCategory> findByIsActive ();
	
	Optional<SkincareCategory> findByCategoryId (Long CategoryId);
}
